/**
 * @module rcli-helper functions
 *
 * utilities to help enable rcli functionality, yeah, that's the ticket...
 */

import { clone, extractErrorMsg, pretty } from '../helpers/utils-helper.mjs';
import { RCLIArgInfo, RCLIArgType, RCLIParamInfo, } from "./rcli-types.mjs";
import { HELPER_LOG_A_LOT } from '../constants.mjs';
import { PARAM_INFO_BARE } from "./rcli-constants.mjs";


/**
 * used in verbose logging
 */
const logalot = HELPER_LOG_A_LOT || false;

/**
 * All incoming arg values are strings. This will convert that depending on the
 * parameter definition.
 *
 * ...hmm, unsure how to handle undefined just yet, even though that is a return type here...
 *
 * @returns casted/parsed value as a string/number/boolean value.
 */
export function getValueFromRawString<T extends RCLIArgType = string>({
    paramInfo,
    valueString,
}: {
    paramInfo: RCLIParamInfo;
    valueString: string | undefined;
}): T | undefined {
    const lc = `[${getValueFromRawString.name}]`;
    try {
        switch (paramInfo.argTypeName) {
            case 'string':
                // no conversion required, but we need to strip the double-quotes if exist.
                let castedValue = valueString as string;
                if (
                    (castedValue.startsWith(`"`) && castedValue.endsWith(`"`)) ||
                    (castedValue.startsWith(`'`) && castedValue.endsWith(`'`))
                ) {
                    castedValue = castedValue.slice(1);
                    castedValue = castedValue.slice(0, castedValue.length - 1);
                }
                return castedValue as T;
            case 'integer':
                // convert to a number
                if (valueString === undefined) { throw new Error(`integer arg value is undefined. integers must be a valid integer string (E: ce17acde3b863ec5e2fdcc594f0f1423)`); }
                const argValueInt = Number.parseInt(valueString);
                if (typeof argValueInt !== 'number') { throw new Error(`arg value string (${valueString})did not parse to an integer. parse result: ${argValueInt} (E: 43cde93160458610ffb49fd16a02d123)`); }
                return argValueInt as T;
            case 'boolean':
                // convert to a boolean
                if (valueString === undefined || valueString === '') {
                    if (!paramInfo.isFlag) { throw new Error(`valueString is undefined or empty string but paramInfo.argTypeName === 'boolean' and paramInfo.isFlag is falsy. (E: 482e595c0ec7344b04def76c1441d623)`); }
                    // value is not provided, so the arg string is empty. the param is
                    // a flag, so just its presence means the value is "true".
                    return true as T;
                } else if (valueString === null) {
                    // ? is this even possible to get here?
                    throw new Error(`(UNEXPECTED) valueString === null? (E: 78f548b93026407968356d9c4f106223)`);
                } else {
                    // typos will evaluate
                    if (!['true', 'false'].includes(valueString)) {
                        throw new Error(`invalid boolean valueString ("${valueString}"). must be either "true" or "false" (E: ba7a0d0804131acc2bd9ab37c0382523)`);
                    }
                    return (valueString === 'true') as T;
                }
            default:
                throw new Error(`(UNEXPECTED) invalid paramInfo.argTypeName (E: c8b03ccb71394d22a29858b98753a123)`);
        }
    } catch (error) {
        console.error(`${lc} ${extractErrorMsg(error)}`);
        throw error;
    }
}

/**
 * extracts the arg value(s) from the corresponding `paramInfo` from the given
 * `argInfos`.
 *
 * @returns the arg.value corresponding to the given `paramInfo`
 */
export function extractArgValue<T extends RCLIArgType>({
    paramInfo,
    argInfos,
    throwIfNotFound,
}: {
    /**
     * Param you're pulling from the args.
     */
    paramInfo: RCLIParamInfo,
    /**
     * the given arg infos from the request line.
     */
    argInfos: RCLIArgInfo<RCLIArgType>[],
    /**
     * If true, will throw an exception if the param is not found in the args,
     * else just returns undefined.
     */
    throwIfNotFound?: boolean,
}): T | T[] | undefined {
    const lc = `[${extractArgValue.name}]`;
    try {
        if (logalot) { console.log(`${lc} starting... (I: d376123d1e383f6323ef1fc6bb68f123)`); }

        const filteredArgInfos = argInfos.filter(x => x.name === paramInfo.name || (x.synonyms ?? []).some(syn => (paramInfo.synonyms ?? []).includes(syn)));

        if (logalot) { console.log(`${lc} filteredArgInfos: ${pretty(filteredArgInfos)} (I: a15831a9bf2930435960263c79d34323)`); }
        if (filteredArgInfos.length === 0) {
            if (throwIfNotFound) {
                throw new Error(`param (name: ${paramInfo.name}) not found among args. (E: a74e41ca7de883f26f216a8d15ab7a23)`);
            } else {
                return undefined;
            }
        }

        if (paramInfo.allowMultiple) {
            // allow multiple args, so return type is T[]
            if (paramInfo.isFlag) { throw new Error(`(UNEXPECTED) param (name: ${paramInfo.name}) is defined as allowMultiple and isFlag, which doesn't make sense. (E: 2854512470b2dde4b9a82fe225d22623)`); }
            if (paramInfo.argTypeName === 'boolean') { throw new Error(`(UNEXPECTED) param (name: ${paramInfo.name}) is defined as allowMultiple and its type name is boolean, which doesn't make sense. (E: 259d77da25374726af4895eb19bb3041)`); }

            if (filteredArgInfos.some(arg => arg.value !== 0 && !arg.value)) {
                throw new Error(`param (name: ${paramInfo.name}) value is not 0 but is falsy. (E: e5af23465f6920a2ff6be7b7d49ef123)`);
            }
            return filteredArgInfos.map(arg => arg.value as T);

        } else {
            // allow only single arg, so return type is T
            if (filteredArgInfos.length > 1) { throw new Error(`param (name: ${paramInfo.name}) had multiple args but param.allowMultiple is falsy. (E: 0d01157e773bd34f962f8713e7719c23)`); }

            const argInfo = filteredArgInfos[0] as RCLIArgInfo<T>;

            // if the flag is set but no `="true"` or `="false"` provided, then
            // we set the value to true
            if (paramInfo.isFlag && argInfo.value === undefined) {
                if (paramInfo.argTypeName !== 'boolean') {
                    throw new Error(`(UNEXPECTED) paramInfo.isFlag is true but argTypeName !== 'boolean' (E: 79a86d0c6ef4c7740aa84211ebadbb23)`);
                }
                argInfo.value = true as T;
            }

            if (logalot) { console.log(`argInfo.value: ${argInfo.value}`) }

            return argInfo.value;
        }
    } catch (error) {
        console.error(`${lc} ${extractErrorMsg(error)}`);
        throw error;
    } finally {
        if (logalot) { console.log(`${lc} complete.`); }
    }
}

/**
 * gets the paramInfo corresponding to the given argIdentifier
 * @returns paramInfo from given `paramInfos` or undefined if not found
 */
export function getParamInfo({
    argIdentifier,
    paramInfos,
    throwIfNotFound,
}: {
    /**
     * arg identifier is either a name or a synonym. atow only a name.
     */
    argIdentifier: string;
    /**
     * All possible param infos that the given arg identifier could be.
     */
    paramInfos: RCLIParamInfo[];
    throwIfNotFound?: boolean;
}): RCLIParamInfo | undefined {
    const lc = `[${getParamInfo.name}]`;
    try {
        const filteredParamInfos = paramInfos.filter(p => p.name === argIdentifier || (p.synonyms ?? []).includes(argIdentifier));

        if (filteredParamInfos.length === 1) {
            return clone(filteredParamInfos[0]);
        } else if (filteredParamInfos.length > 1) {
            throw new Error(`(UNEXPECTED) multiple param infos found with argIdentifier. do you have overlapping arg names/synonyms? (${argIdentifier} found in (${filteredParamInfos.length} param infos)) (E: d599a6647c5ead6d9fbac4e4c96e6d23)`);
        } else {
            if (throwIfNotFound) {
                throw new Error(`(UNEXPECTED) param info not found for argIdentifier (${argIdentifier}) (E: 47e704068f2eb5a0551cf45d0e72c823)`);
            } else {
                return undefined;
            }
        }
    } catch (error) {
        console.error(`${lc} ${extractErrorMsg(error)}`);
        throw error;
    }
}

/**
 * helper that checks a raw arg (including any dashes) against a parameter
 * definition.
 *
 * @returns true if the arg is the given paramInfo, else false
 */
export function argIs({ arg, paramInfo, }: {
    /**
     * raw arg string, including dash(es)
     */
    arg: string,
    /**
     * param you're checking against the raw arg value.
     */
    paramInfo: RCLIParamInfo,
}): boolean {
    const bareArg = arg.replace('--', '').toLowerCase();
    const nameMatches = bareArg === paramInfo.name.toLowerCase();
    if (nameMatches) {
        // no need to check synonyms
        return true; /* <<<< returns early */
    } else if ((paramInfo.synonyms ?? []).length > 0) {
        // check synonyms
        return paramInfo.synonyms!.some(x => x.toLowerCase() === bareArg);
    } else {
        // no synonyms and name didn't match
        return false;
    }
}

/**
 * This takes incoming raw `args` and parameter definitions and
 * creates arginfos based on the given args.
 * @returns argInfos that include values based on given raw args
 */
export function buildArgInfos<T extends RCLIArgType = string>({
    args,
    paramInfos,
    bareArgParamInfo = clone(PARAM_INFO_BARE),
    logalot: localLogalot,
}: {
    /**
     * array of raw args, including dashes
     */
    args: string[],
    /**
     * parameter set of all possible parameters
     */
    paramInfos: RCLIParamInfo[],
    /**
     * provides the param info to use when encountering a "bare arg".
     *
     * there can be one bare arg in the RCLI args list. This is an arg that does
     * not start with the `--` prefix. When a bare arg is received, you must
     * specify which param to map this to, because we don't have a param
     * identifer (param name or synonym).
     */
    bareArgParamInfo?: RCLIParamInfo,
    /**
     * if you want to use verbose logging, set this to true
     */
    logalot?: boolean,
}): RCLIArgInfo<T>[] {
    const lc = `[${buildArgInfos.name}]`;
    try {
        if (logalot || localLogalot) { console.log(`${lc} starting... (I: f389aaa490796dfd823bc7b9d4e58b23)`); }

        /**
         * only one bare arg is allowed. if it has spaces, must be inside quotes.
         *
         * if a bare arg is found and we then find another one, then we gotta throw
         */
        let bareArgFound = false;

        const argInfos = args.map((arg: string) => {
            let argIdentifier: string;
            let valueString: string;
            let argInfo: RCLIArgInfo<T>;
            if (arg.startsWith("--")) {
                // normal arg prefixed with --
                arg = arg.slice(2);
                if (logalot || localLogalot) { console.log(`arg sans dashes: ${arg}`); }

                if (arg.includes('=')) {
                    if (logalot) { console.log(`${lc} arg with equals: ${arg}`) }
                    [argIdentifier, valueString] = arg.split('=');
                    const paramInfo = getParamInfo({ argIdentifier, paramInfos, throwIfNotFound: true })!;
                    argInfo = {
                        ...paramInfo,
                        value: getValueFromRawString<T>({ paramInfo, valueString }),
                        identifier: argIdentifier,
                    }
                } else {
                    if (logalot) { console.log(`${lc} arg without equals: ${arg}`) }
                    argIdentifier = arg;
                    const paramInfo = getParamInfo({ argIdentifier, paramInfos, throwIfNotFound: true })!;
                    argInfo = {
                        ...paramInfo,
                        value: getValueFromRawString<T>({ paramInfo, valueString: undefined }),
                        identifier: argIdentifier,
                        isFlag: true, // should be the same in paramInfo, but being explicit here
                    }
                }
            } else {
                // bare arg found
                if (logalot || localLogalot) { console.log(`bare arg found. (I: d6f67074c7e44a63864948d4bf04bee8)`); }
                if (!bareArgParamInfo) { throw new Error(`bare arg found (one without "--" prefix) but there is no param info expected for bare args. You must provide which param info a bare arg maps to in this ${buildArgInfos.name} call. (E: 650dd3695d62f0f87dd0cbafbf068c23)`); }
                if (bareArgFound) { throw new Error(`more than one bare arg (one without "--" prefix) found. you can only have one bare arg and if it has spaces then you have to enclose it with single or double quotes. (E: 74dea8f13f0c14e3b7a2f125c7faca23)`); }
                bareArgFound = true;
                const paramInfo = clone(bareArgParamInfo) as RCLIParamInfo;
                argInfo = {
                    ...paramInfo,
                    isBare: true,
                    identifier: paramInfo.name, // not strictly true but the isBare indicates this
                    value: getValueFromRawString<T>({ paramInfo, valueString: arg }),
                };
            }
            if (logalot) { console.log(`${lc} argInfo: ${pretty(argInfo)}`); }
            return argInfo!;
        });
        return argInfos;
    } catch (error) {
        console.error(`${lc} ${extractErrorMsg(error)}`);
        throw error;
    } finally {
        if (logalot || localLogalot) { console.log(`${lc} complete.`); }
    }
}
