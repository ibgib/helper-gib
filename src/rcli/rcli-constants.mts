import { RCLIParamInfo } from "./rcli-types.mjs";

/**
 * special param that is used as the only param allowed that doesn't have a
 * "double-dash-specifier=" form. one "bare" arg is allowed atow. if a bare arg
 * is found, it will be mapped to this param info.
 */
export const PARAM_INFO_BARE: RCLIParamInfo = {
    name: 'bare',
    description: `special param that is used as the only param allowed that doesn't have a "double-dash-specifier=" form. one "bare" arg is allowed atow. if a bare arg is found, it will be mapped to this param info.`,
    synonyms: ['bare-arg', 'bare-param', 'no-name', 'manco', 'arg-with-no-name', 'param-with-no-name'],
    argTypeName: 'string',
};
/**
 * the most essential parameter in existence...we all need it.
 */
export const PARAM_INFO_HELP: RCLIParamInfo = {
    name: 'help',
    description: 'the most essential parameter in existence...we all need it.',
    synonyms: ['h'],
    isFlag: true,
    argTypeName: 'boolean',
};
/**
 * flag for dry-run (not producing output but simulating what WOULD be produced)
 */
export const PARAM_INFO_DRY_RUN: RCLIParamInfo = {
    name: 'dry-run',
    description: 'flag for dry-run (not producing output but simulating what WOULD be produced)',
    synonyms: ['dry'],
    isFlag: true,
    argTypeName: 'boolean',
};
/**
 * path pointing to a data resource (file/folder)
 */
export const PARAM_INFO_DATA_PATH: RCLIParamInfo = {
    name: 'data-path',
    description: 'path pointing to a data resource (file/folder)',
    synonyms: [],
    argTypeName: 'string',
};
/**
 * for referencing/ingesting file(s)/folder(s) in a fs
 */
export const PARAM_INFO_INPUT_PATH: RCLIParamInfo = {
    name: 'input-path',
    description: 'for referencing/ingesting file(s)/folder(s) in a fs',
    synonyms: ['in', 'in-path', 'dir-path', 'folder-path', 'from-path', 'src-path'],
    argTypeName: 'string',
};
/**
 * for generating file(s)/folder(s)
 */
export const PARAM_INFO_OUTPUT_PATH: RCLIParamInfo = {
    name: 'output-path',
    description: 'for indicating where to put generated file(s)/folder(s)',
    synonyms: ['out', 'out-path', 'to-path', 'dest-path'],
    argTypeName: 'string',
};
/**
 * catchall data as a string parameter
 */
export const PARAM_INFO_DATA_STRING: RCLIParamInfo = {
    name: 'data-string',
    description: 'catchall data as a string parameter',
    synonyms: ['ds'],
    argTypeName: 'string',
};
/**
 * catchall data as an integer parameter
 */
export const PARAM_INFO_DATA_INTEGER: RCLIParamInfo = {
    name: 'data-integer',
    description: 'catchall data as an integer parameter',
    synonyms: ['integer', 'int', 'data-number', 'number', 'num'],
    argTypeName: 'integer',
};
/**
 * catchall data as a boolean parameter
 */
export const PARAM_INFO_DATA_BOOLEAN: RCLIParamInfo = {
    name: 'data-boolean',
    description: 'catchall data as an integer parameter',
    synonyms: ['boolean', 'bool', 'data-bool'],
    argTypeName: 'integer',
};
/**
 * used for when you have a name of whatever, based on command/context.
 *
 * i'm adding this for `PARAM_INFO_GENERATE_SOURCE_FILE` (downstream in
 * ibgib/rcli app), but should be reusable.
 */
export const PARAM_INFO_NAME: RCLIParamInfo = {
    name: 'name',
    description: 'specify the name of something determined by context of the command.',
    argTypeName: 'string',
    allowMultiple: false,
    synonyms: ['n'],
};

/**
 * Array of common parameters.
 */
export const COMMON_PARAM_INFOS: RCLIParamInfo[] = [
    PARAM_INFO_BARE,
    PARAM_INFO_HELP,
    PARAM_INFO_DRY_RUN,
    PARAM_INFO_DATA_PATH,
    PARAM_INFO_INPUT_PATH,
    PARAM_INFO_OUTPUT_PATH,
    PARAM_INFO_DATA_STRING,
    PARAM_INFO_DATA_INTEGER,
    PARAM_INFO_DATA_BOOLEAN,
    PARAM_INFO_NAME,
];
